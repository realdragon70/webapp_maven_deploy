.
├── a
├── b
├── c
├── multi3
│   ├── pom.xml
│   ├── src
│   │   ├── main
│   │   │   ├── java
│   │   │   │   └── artifactory
│   │   │   │       └── test
│   │   │   │           └── Multi3.java
│   │   │   └── webapp
│   │   │       ├── WEB-INF
│   │   │       │   └── web.xml
│   │   │       ├── images
│   │   │       │   └── finfra.png
│   │   │       └── index.html
│   │   └── test
│   │       └── java
│   │           └── artifactory
│   │               └── test
│   │                   └── AppTest.java
│   └── target
│       ├── classes
│       │   └── artifactory
│       │       └── test
│       │           └── Multi3.class
│       ├── maven-status
│       │   └── maven-compiler-plugin
│       │       ├── compile
│       │       │   └── default-compile
│       │       │       ├── createdFiles.lst
│       │       │       └── inputFiles.lst
│       │       └── testCompile
│       │           └── default-testCompile
│       │               ├── createdFiles.lst
│       │               └── inputFiles.lst
│       ├── multi3-3.9-jgnam
│       │   ├── META-INF
│       │   ├── WEB-INF
│       │   │   ├── classes
│       │   │   │   └── artifactory
│       │   │   │       └── test
│       │   │   │           └── Multi3.class
│       │   │   ├── lib
│       │   │   │   └── hsqldb-1.8.0.10.jar
│       │   │   └── web.xml
│       │   ├── images
│       │   │   └── finfra.png
│       │   └── index.html
│       ├── multi3-3.9-jgnam.war
│       ├── surefire-reports
│       │   ├── TEST-artifactory.test.AppTest.xml
│       │   └── artifactory.test.AppTest.txt
│       └── test-classes
│           └── artifactory
│               └── test
│                   └── AppTest.class
└── pom.xml

35 directories, 24 files
